---
banner:
  enable: true
  bg_image: images/background/banner.png
  title: Build Your Business Together
  watermark: techviti
  content: We bring the right solution to your business with innovative solutions
    with the right strategy where companies thrive with technology and extend their
    business.
  image: images/banner/banner-1.png
  button:
    enable: true
    label: Contact Techviti
    link: "#"
partner:
  enable: true
  partner_logo:
  - images/partner/client-logo-1.png
  - images/partner/client-logo-2.png
  - images/partner/client-logo-3.png
  - images/partner/client-logo-4.png
  - images/partner/client-logo-5.png
feature:
  enable: true
  subtitle: ''
  title: We’ve many services to solve problems.
  feature_item:
  - name: Free Trial
    image: images/feature/feature-1.png
    content: Web development is everything involved in the creation of a website.
      Typically it refers to the coding and programming side of web site production
      as opposed to the web design side
    title: Web Development
  - name: No Setup
    image: images/feature/feature-2.png
    content: A mobile application (also called a mobile app) is a type of application
      designed to run on a mobile device, which can be a smartphone or tablet computer.
    title: Mobile Application
  - name: Optimized Data
    image: images/feature/feature-3.png
    content: Digital marketing, also called online marketing, is the promotion of
      brands to connect with potential customers using the internet and other forms
      of digital communication
    title: Digital Marketing
about:
  enable: true
  about_item:
  - image: images/about/about-1.png
    subtitle: about agico
    title: Eausmod tempor magna nostrud exercitation
    content: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
      quis nostrud exercitation ullamco laboris nisi aliquip commodo consequat. duis
      aute.
    button:
      enable: true
      label: read more
      link: about
  - image: images/about/about-2.png
    subtitle: about agico
    title: Rehenderit volupate velit proident sunt culpa
    content: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
      quis nostrud exercitation ullamco laboris nisi aliquip commodo consequat. duis
      aute.
    button:
      enable: true
      label: read more
      link: about
promo_video:
  enable: false
  video_thumbnail: images/check-video.png
  video_link: https://www.youtube.com/watch?v=VufDd-QL1c0
  subtitle: Check Video
  title: Best Way to Chat Your Customers.
  content: |-
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi aliquip commodo consequat. duis aute.

    Reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident.
testimonial:
  enable: false
  subtitle: testimonial
  title: What Our Clients Says?
  testimonial_item:
  - name: Alice kelly
    designation: Happy client
    content: Lorem ipsum dolor amet constur adipi sicing elit sed eiusmtempor incid
      dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris
      nisi ut aliquip excepteur sint occaecat cuida tat nonproident sunt in culpa
      qui officia deserunt mollit anim id est laborum. sed ut perspiciatis.
  - name: Alice kelly
    designation: Happy client
    content: Lorem ipsum dolor amet constur adipi sicing elit sed eiusmtempor incid
      dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris
      nisi ut aliquip excepteur sint occaecat cuida tat nonproident sunt in culpa
      qui officia deserunt mollit anim id est laborum. sed ut perspiciatis.
  - name: Alice kelly
    designation: Happy client
    content: Lorem ipsum dolor amet constur adipi sicing elit sed eiusmtempor incid
      dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris
      nisi ut aliquip excepteur sint occaecat cuida tat nonproident sunt in culpa
      qui officia deserunt mollit anim id est laborum. sed ut perspiciatis.
  - name: Alice kelly
    designation: Happy client
    content: Lorem ipsum dolor amet constur adipi sicing elit sed eiusmtempor incid
      dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris
      nisi ut aliquip excepteur sint occaecat cuida tat nonproident sunt in culpa
      qui officia deserunt mollit anim id est laborum. sed ut perspiciatis.
  - name: Alice kelly
    designation: Happy client
    content: Lorem ipsum dolor amet constur adipi sicing elit sed eiusmtempor incid
      dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris
      nisi ut aliquip excepteur sint occaecat cuida tat nonproident sunt in culpa
      qui officia deserunt mollit anim id est laborum. sed ut perspiciatis.
download:
  enable: false
  title: Download Agico Now
  image: images/download-mobile-img.jpg
  content: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis
    nostrud exercitation.
  playstore:
    label: Google Play
    link: "#"
  appstore:
    label: app store
    link: "#"
pricing:
  enable: false
  section: pricing
  show_items: "3"

---
